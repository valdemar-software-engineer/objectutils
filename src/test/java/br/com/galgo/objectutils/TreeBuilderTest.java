package br.com.galgo.objectutils;

import br.com.stianbid.schemafundo.MessageFundoComplexType;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by valdemar.arantes on 13/04/2015.
 */
public class TreeBuilderTest {

    private static final Logger log = LoggerFactory.getLogger(TreeBuilder.class);

    @Test
    public void testBuildTree() throws Exception {
        TreeBuilder treeBuilder =  TreeBuilder.from(MessageFundoComplexType.class, "fundo");
        Tree<TreeBuilder.Node> tree = treeBuilder.build();

        List<TreeBuilder.Node> leaves = tree.getLeaves(tree.getHead());
        log.debug("{} folhas encontradas na árvore", leaves.size());

        try {
            // Logando todas as propriedades aninhadas das folhas da árvores de classes
            int i = 0;
            StringBuilder buff = new StringBuilder();
            final String fmt = "%s; %s; %s; %s\n";
            for (TreeBuilder.Node leaf : leaves) {
                String nestePropertyName = treeBuilder.getNestePropertyName(leaf);
                i++;
                buff.append(String.format(fmt, i, leaf.propName, leaf.propClass.getName(), nestePropertyName));
            }
            log.debug("Lista de propriedades aninhadas:\n{}", buff);
        } catch (Exception e) {
            log.error(null, e);
        }
    }

    @Test
    @Ignore
    public void testAddNode() throws Exception {

    }

    @Test
    @Ignore
    public void testGetNestedPropertyName() throws Exception {

    }

    @Test
    @Ignore
    public void testGetNestePropertyName() throws Exception {

    }

    @Test
    @Ignore
    public void testGetChildren() throws Exception {

    }
}