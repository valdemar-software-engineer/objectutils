/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.objectutils;

import com.google.common.collect.Lists;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class JsonSerializerTest {

    private static final Logger log = LoggerFactory.getLogger(JsonSerializerTest.class);

    @Test
    public void test() {
        Teste teste = new Teste();
        log.info("teste={}", ToStringBuilder.reflectionToString(teste));
        String tJson = JsonSerializer.serialize(teste);
        log.info("tJson={}", tJson);
        Teste tDeser = JsonSerializer.deserialize(tJson, Teste.class);
        log.info("tDeser={}", ToStringBuilder.reflectionToString(tDeser));

        Assert.assertTrue(teste.dateTime.getMillis() == tDeser.dateTime.getMillis());
        Assert.assertTrue(teste.gregDt.equals(tDeser.gregDt));
    }

    static class Teste {

        private XMLGregorianCalendar gregDt;
        private DateTime dateTime;

        public Teste() {
            try {
                gregDt = DatatypeFactory.newInstance().newXMLGregorianCalendar(
                        1967, 3, 28, 15, 0, 0, 0, -3);
                dateTime = new DateTime(1979, 11, 22, 9, 0);
            } catch (DatatypeConfigurationException ex) {
                ex.printStackTrace();
            }
        }
    }

}
