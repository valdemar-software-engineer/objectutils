/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.galgo.objectutils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import java.lang.reflect.Type;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author valdemar.arantes
 */
public class XMLGregorianCalendarConverter {
    public static class Serializer implements com.google.gson.JsonSerializer<XMLGregorianCalendar> {

        public JsonElement serialize(XMLGregorianCalendar src, Type type,
                JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(src.toXMLFormat());
        }
    }

    public static class Deserializer implements JsonDeserializer {

        public XMLGregorianCalendar deserialize(JsonElement jsonElement, Type type,
                JsonDeserializationContext jsonDeserializationContext) {
            try {
                return DatatypeFactory.newInstance().newXMLGregorianCalendar(
                        jsonElement.getAsString());
            } catch (Exception e) {
                return null;
            }
        }
    }
}
