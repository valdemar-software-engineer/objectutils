package br.com.galgo.objectutils;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

/**
 * Created by valdemar.arantes on 13/04/2015.
 */
public class TreeBuilder {

    private static final Logger log = LoggerFactory.getLogger(TreeBuilder.class);
    private Tree<Node> t;
    private final Class<?> beanClass;
    private final String beanPropName;

    public static class Node {

        public String propName;
        public Class<?> propClass;
        public boolean isList;

        public Node(String propName, Class<?> propClass) {
            this.propName = propName;
            this.propClass = propClass;
        }

        @Override
        public String toString() {
            return String.format("%1s%2s | %3s", propName, isList ? "[]" : "",
                    propClass == null ? "{NULL}" : propClass.getName());
        }
    }

    private TreeBuilder(Class<?> beanClass, String beanName) {
        this.beanClass = beanClass;
        this.beanPropName = beanName;
    }

    public static TreeBuilder from(Class<?> beanClass, String beanName) {
        return new TreeBuilder(beanClass, beanName);
    }

    public Tree<Node> build() {
        log.info("Construindo a árvores de classes...");
        XmlType xmlType = beanClass.getAnnotation(XmlType.class);
        Node root = new Node(beanPropName, beanClass);
        addNode(null, root);
        log.debug(t.toString());
        return t;
    }

    public Tree<Node> addNode(Tree<Node> t1, Node node) {
        Tree<Node> ret;
        log.debug("Adicionando o nó {} ao pai {}", node, t1);
        if (t1 == null) {
            ret = new Tree<>(node);
            t = ret;
        } else {
            ret = t1.addLeaf(node);
        }

        if (t1 != null) {
            Field f = null;
            Class<?> beanClass = t1.getHead().propClass;
            if (isListField(beanClass, node.propName)) {
                Class<?> aClass = getGenericTypeFromField(beanClass, node.propName);
                node.isList = true;
                if (aClass != null) {
                    node.propClass = aClass;
                }
            }
        }

        if (node.isList) {
            int i = node.getClass().getTypeParameters().length;
        }

        List<Node> children = getChildren(node.propClass);

        if (CollectionUtils.isEmpty(children)) {
            return ret;
        }

        for (Node child : children) {
            addNode(ret, child);
        }

        return ret;
    }

    /**
     * @param subTree
     * @return Uma string com o nome aninhado da propriedade associada à raiz da árvore subTree
     */

    public String getNestedPropertyName(Tree<Node> subTree) {
        String ret = "";

        if (subTree == null) {
            return null;
        }

        if (subTree.getParent() == null) {
            // Estamos na raiz da árvore à qual esta subTree deveria pertencer
            if (subTree != t) {
                // Inválida! Esta sub árvore não pertence à árvore t desta TreeBuilder
                throw new RuntimeException(String.format("Sub-árvore %1s não pertence à árvore %2s", subTree, t));
            }

            return subTree.getHead().propName;
        }

        Node stHead = subTree.getHead();
        ret = getNestedPropertyName(subTree.getParent()) + "." + stHead.propName + (stHead.isList ? "[]" : "");
        return ret;
    }

    /**
     * @param n
     * @return Uma string com o nome aninhado da propriedade
     */
    public String getNestePropertyName(final Node n) {
        String ret = "";
        Node actualNode = n;
        do {
            ret = actualNode.propName + (actualNode.isList ? "[]" : "") + "." + ret;
            Tree<Node> parent = t.getTree(actualNode).getParent();
            if (parent == null) {
                break;
            }
            actualNode = parent.getHead();
        } while (true);

        // Removendo o "." que fica no fim da String por conta do algoritmo acima
        return ret.substring(0, ret.length() - 1);
    }

    public static List<Node> getChildren(Class<?> propClass) {
        log.debug("getChildren() - propClass={}", propClass);
        XmlType xmlType = propClass.getAnnotation(XmlType.class);
        if (xmlType == null) {
            return null;
        }

        String[] propOrder = xmlType.propOrder();
        if (propOrder == null || propOrder.length == 0 || (propOrder.length == 1 && StringUtils.isBlank(propOrder[0]))) {
            log.info("propOrder da anotação XmlType da classe {} não está definido");
            return null;
        }

        log.debug("propOrder={}", Arrays.toString(propOrder));

        // Criando um mapa propName X propType da classe propClass
        Field[] fields = propClass.getDeclaredFields();
        //Map<String, Class<?>> xmlElementName2TypeMap = Maps.newHashMap();
        StringBuilder buff = new StringBuilder();
        final String fmt = "%s=%s\n";
        List<Node> nodeList = Lists.newArrayList();
        for (Field field : fields) {
            XmlElement xmlElement = field.getAnnotation(XmlElement.class);
            if (xmlElement == null) {
                continue;
            }
            nodeList.add(new Node(field.getName(), field.getType()));
            //xmlElementName2TypeMap.put(xmlElement.name(), field.getType());
            buff.append(String.format(fmt, xmlElement.name(), field.getType()));
        }
        log.debug(buff.toString());

/*
        PropertyDescriptor[] descrList = PropertyUtils.getPropertyDescriptors(propClass);
        Map<String, Class<?>> propName2propType = new HashMap<>();
        for (PropertyDescriptor descr : descrList) {
            propName2propType.put(descr.getName(), descr.getPropertyType());
            String propXmlElement = "";
            try {
                propXmlElement = propClass.getDeclaredField(descr.getName()).getAnnotation(XmlElement.class).name();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            buff.append(String.format(fmt, propXmlElement, descr.getName(), descr.getPropertyType()));
        }
*/

/*
        List<Node> nodeList = new ArrayList<>(propOrder.length);
        for (String prop : propOrder) {
            if (xmlElementName2TypeMap.get(prop) == null) {
                log.error("prop={} não encontrada no map propName2propType", prop);
            }
            nodeList.add(new Node(prop, xmlElementName2TypeMap.get(prop)));
        }
*/

        return nodeList;
    }

    /**
     * @param clazz
     * @param fieldName
     * @return true se o campo for uma propriedade do tipo Lista com tipo genérico na classe clazz
     */
    private boolean isListField(Class<?> clazz, String fieldName) {
        try {
            Field f = clazz.getDeclaredField(fieldName);
            return f.getType().equals(List.class);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param clazz
     * @param fieldName
     * @return Retorna o tipo genérico associado à declaração do campo fieldName
     */
    private Class<?> getGenericTypeFromField(Class<?> clazz, String fieldName) {
        try {
            Field f = clazz.getDeclaredField(fieldName);
            Type tp = f.getGenericType();
            if (tp instanceof ParameterizedType) {
                ParameterizedType pt = (ParameterizedType) tp;
                Type[] actualTypes = pt.getActualTypeArguments();
                Class<?> aClass = (Class<?>) actualTypes[0];
                return aClass;
            } else {
                log.info("O campo {} não foi declarado com classe genérica na classe {}", fieldName,
                        clazz.getName());
                return null;
            }
        } catch (Exception e) {
            log.error(null, e);
            return null;
        }
    }
}
