package br.com.galgo.objectutils;

import com.google.common.collect.Lists;

import java.util.*;

/**
 * Created by valdemar.arantes on 13/04/2015.
 */
public class Tree<T> {
    private T head;
    private List<Tree<T>> leafs;
    private Tree<T> parent;
    private Map<T, Tree<T>> locate;

    public Tree(T head) {
        this.head = head;
        leafs = new ArrayList<>();
        locate = new LinkedHashMap<>();
        locate.put(head, this);
    }

    public Tree<T> addLeaf(T root, T leaf) {
        if (locate.containsKey(root)) {
            return locate.get(root).addLeaf(leaf);
        } else {
            return addLeaf(root).addLeaf(leaf);
        }
    }

    public Tree<T> addLeaf(T leaf) {
        Tree<T> t = new Tree<>(leaf);
        leafs.add(t);
        t.parent = this;
        t.locate = this.locate;
        locate.put(leaf, t);
        return t;
    }

    public Tree<T> addLeaves(T... leaves) {
        Tree<T> ret = null;
        for (T leaf : leaves) {
            ret = addLeaf(leaf);
        }

        return ret;
    }

    public Tree<T> setAsParent(T parentRoot) {
        Tree<T> t = new Tree<>(parentRoot);
        t.leafs.add(this);
        this.parent = t;
        t.locate = this.locate;
        t.locate.put(head, this);
        t.locate.put(parentRoot, t);
        return t;
    }

    public T getHead() {
        return head;
    }

    public Tree<T> getTree(T element) {
        return locate.get(element);
    }

    public Tree<T> getParent() {
        return parent;
    }

    public List<T> getSuccessors(T root) {
        List<T> successors = new ArrayList<>();
        Tree<T> tree = getTree(root);
        if (null != tree) {
            for (Tree<T> leaf : tree.leafs) {
                successors.add(leaf.head);
            }
        }
        return successors;
    }

    public List<Tree<T>> getSubTrees() {
        return leafs;
    }

    /**
     *
     * @param root
     * @return Lista dos elementos que são folhas, isto é, que não possuem filhos
     */
    public List<T> getLeaves(T root) {
        List<T> leaves = new ArrayList(0);

        List<T> childs = getSuccessors(root);
        if (childs.isEmpty()) {
            leaves.add(root);
        } else {
            for (T child : childs) {
                leaves.addAll(getLeaves(child));
            }
        }

        return leaves;
    }

    /**
     *
     * @param rootTree
     * @return Lista das árvores que são folhas, isto é, que não possuem filhos
     */
    public List<Tree<T>> getLeaveTrees(Tree<T> rootTree) {
        List<Tree<T>> leaveTrees = Lists.newArrayList();
        if (rootTree == null) {
            return leaveTrees;
        }

        List<Tree<T>> subTrees = rootTree.getSubTrees();
        if (subTrees == null || subTrees.isEmpty()) {
            leaveTrees.add(rootTree);
        } else {
            for (Tree<T> subTree : subTrees) {
                leaveTrees.addAll(getLeaveTrees(subTree));
            }
        }
        return leaveTrees;
    }

    /**
     *
     * @return true se esta árvore for a primeira sucessora do árvore pai.
     */
    public boolean isFirstSuccessor() {
        return getParent().getSubTrees().get(0) == this;
    }

    public static <T> Collection<T> getSuccessors(T of, Collection<Tree<T>> in) {
        for (Tree<T> tree : in) {
            if (tree.locate.containsKey(of)) {
                return tree.getSuccessors(of);
            }
        }
        return new ArrayList<T>();
    }

    @Override
    public String toString() {
        return printTree(0);
    }

    private static final int indent = 2;

    private String printTree(int increment) {
        String s = "";
        String inc = "";
        for (int i = 0; i < increment; ++i) {
            inc = inc + " ";
        }
        s = inc + head;
        for (Tree<T> child : leafs) {
            s += "\n" + child.printTree(increment + indent);
        }
        return s;
    }
}
