/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.galgo.objectutils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import java.lang.reflect.Type;
import javax.xml.datatype.XMLGregorianCalendar;
import org.joda.time.DateTime;

/**
 *
 * @author valdemar.arantes
 */
public class JsonSerializer {
    public static String serialize(Object o) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        registerAdapters(gsonBuilder);
        Gson gson = gsonBuilder.create();
        return gson.toJson(o);
    }

    public static <T> T deserialize(String json, Class<T> classOfT) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        registerAdapters(gsonBuilder);
        Gson gson = gsonBuilder.create();
        return gson.fromJson(json, classOfT);
    }

    private static void registerAdapters(GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeAdapter(DateTime.class,
                new com.google.gson.JsonSerializer<DateTime>() {

                    @Override
                    public JsonElement serialize(DateTime src, Type typeOfSrc,
                            JsonSerializationContext context) {
                        return new JsonPrimitive(src.toString());
                    }
                });
        gsonBuilder.registerTypeAdapter(DateTime.class,
                new com.google.gson.JsonDeserializer<DateTime>() {

                    @Override
                    public DateTime deserialize(JsonElement json, Type typeOfT,
                            JsonDeserializationContext context) throws JsonParseException {
                        return DateTime.parse(json.getAsString());
                    }

                });

        gsonBuilder.registerTypeHierarchyAdapter(XMLGregorianCalendar.class,
                new XMLGregorianCalendarConverter.Serializer());
        gsonBuilder.registerTypeAdapter(
                XMLGregorianCalendar.class, new XMLGregorianCalendarConverter.Deserializer());
    }

}
